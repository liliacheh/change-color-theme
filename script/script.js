let link = document.querySelector(".main-theme")
let change_btn = document.querySelector(".change_btn");
change_btn.addEventListener('click', changeTheme)
 if (localStorage.getItem("new-theme") === "true") {
    link.remove() 
    document.head.insertAdjacentHTML('beforeend', `<link class ="new-theme" rel="stylesheet" href="./style/new-theme.css">`)  
 }
function changeTheme(){
    if(localStorage.getItem("new-theme") !== "true"){
        let link = document.querySelector(".main-theme")
        link.remove() 
        document.head.insertAdjacentHTML('beforeend', `<link class ="new-theme" rel="stylesheet" href="./style/new-theme.css">`) 
        localStorage.setItem("new-theme", "true")
    } else {
        let link1 = document.querySelector(".new-theme")
        link1.remove()         
        document.head.insertAdjacentHTML('beforeend',`<link class ="main-theme" rel="stylesheet" href="./style/style.css">`)
        localStorage.setItem("new-theme", "false")   
}
}
